# Use http://speccy.io/ to lint this file and to merge it into the general
# sensorbucket openapi specification

openapi: '3.0.2'
info:
  title: SensorBucket Identity Server
  version: '0.0.1'
servers:
  - description: Production
    url: https://sensorbucket.nl/api/v1
  - description: Staging
    url: https://sensorbucket.pollex.nl/api/v1
  - description: Local
    url: http://localhost:3000

# The authentication header is added to each request for simplicity
security:
  - bearerAuth: []

components:
  schemas:  
    UserCreation:
      properties:
        mail:
          type: string
          example: user@sensorbucket.nl
        firstname:
          type: string
          example: John
        lastname:
          type: string
          example: Doe
        password:
          type: string
          example: Sup3rStrongP4ssword!
    User:
      properties:
        id:
          type: string
          example: dad9dedb-9781-4ba1-842a-ea999e7479e1
        mail:
          type: string
          example: user@sensorbucket.nl
        firstname:
          type: string
          example: John
        lastname:
          type: string
          example: Doe
    OrganisationCreation:
      properties:
        name:
          type: string
          example: TheSensingGroup
        address:
          type: string
          example: Fake Street 45
        zipcode:
          type: string
          example: 1234AB
        city:
          type: string
          example: Middelburg
    Organisation:
      properties:
        id:
          type: string
          example: dad9dedb-9781-4ba1-842a-ea999e7479e1
        name:
          type: string
          example: TheSensingGroup
        address:
          type: string
          example: Fake Street 45
        zipcode:
          type: string
          example: 1234AB
        city:
          type: string
          example: Middelburg
        archiveTime:
          type: integer
          example: 168
          description: The amount of hours that sensor data will be stored for
        createdAt:
          type: string
          format: ISO-8601
          example: 2021-01-31T18:31:33.987430435Z
    APIError:
      properties:
        message:
          type: string
        code:
          type: string
    APIResponse:
      properties:
        message:
          type: string
        data: {}
  responses:
    Unauthorized:
      description: Request is not authenticated
      content:
        application/json:
          schema:
            properties:
              message:
                type: string
                example: Authentication required
              code:
                type: string
                example: UNAUTHORIZED
    Forbidden:
      description: User is not allowed to make this request
      content:
        application/json:
          schema:
            properties:
              message:
                type: string
                example: Not authorized to perform this action
              code:
                type: string
                example: FORBIDDEN
  parameters:
    id:
      name: uuid
      in: path
      required: true
      schema:
        type: string
        format: UUID
        example: dad9dedb-9781-4ba1-842a-ea999e7479e1
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT

paths: 
  /auth/login:
    post:
      summary: Authenticates a user and returns an authentication token
      requestBody:
        required: true
        content:
          application/json:
            schema:
              properties:
                email:
                  type: string
                  example: user@sensorbucket.nl
                password:
                  type: string
                  example: Sup3rStrongP4ssword!
      responses:
        '200':
          description: Authentication and refresh token
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Succesfully logged in"
                  data:
                    type: object
                    properties:
                      authenticationToken:
                        type: string
                      refreshToken:
                        type: string
        '403':
          description: Failed authentication
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                    example: "Incorrect username or password"
                  code:
                    type: string
                    example: AUTH_INVALID_CREDENTIALS

  /users:
    post:
      tags:
        - users
      summary: Create a new user account     
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref:  "#/components/schemas/UserCreation"     
      responses:
        '201':
          description: Succesfully registered user
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Succesfully registered user
                  data:
                    $ref:  "#/components/schemas/User"
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: The provided email address is already in use
                  code:
                    type: string
                    example: DUPLICATE_USER_EMAIL
  
  /users/{id}:
    get:
      tags:
        - users
      summary: Get a user by its ID
      parameters:
        - $ref: "#/components/parameters/uuid"
      responses:
        '200':
          description: Succesfully fetched user
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Succesfully fetched user
                  data:
                    $ref:  "#/components/schemas/User"
  
  /users/self:
    get:
      tags:
        - users
      summary: Get the currently authenticate user
      responses:
        '200':
          description: Current user succesfully fetched
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Succesfully fetched user
                  data:
                    $ref:  "#/components/schemas/User"

  /organisations:
    post:
      tags:
        - organisations
      summary: Create a new organisation
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref:  "#/components/schemas/OrganisationCreation"
      responses:
        '201':
          description: Created a new organisation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/OrganisationCreation"
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: An organisation with this name already exists
                  code:
                    type: string
                    example: ORGANISATION_DUPLICATE_NAME
            
  /organisations/{id}:
    get:
      tags:
        - organisations
      summary: Get an organisation by its ID
      parameters:
        - $ref: "#/components/parameters/uuid"
      responses:
        '200':
          description: Succesfully fetched organisation by ID
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Succesfully fetched members
                  data:
                    $ref:  "#/components/schemas/Organisation"
        '404':
          description: Organisation not found
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Organisation was not found
                  code:
                    type: string
                    example: ORGANISATION_NOT_FOUND

  /organisations/{id}/members:
    get:
      tags:
        - organisations
      summary: Get all organisation members
      parameters:
        - $ref: "#/components/parameters/uuid"
      responses:
        '200':
          description: Succesfully fetched members
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Succesfully fetched members
                  data:
                    type: array
                    items:
                      $ref:  "#/components/schemas/User"
        '404':
          description: Organisation not found
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Organisation was not found
                  code:
                    type: string
                    example: ORGANISATION_NOT_FOUND
  
  /organisations/{id}/members/{uid}:
    delete:
      tags:
        - organisations
      summary: Remove a member from the organisation
      description: |
        Currently members cannot be removed except for the user itself. In other
        words, the user can 'leave' the organisation.
      parameters:
        - $ref: "#/components/parameters/uuid"
        - name: uid
          in: path
          required: true
          schema:
            type: string
            format: UUID
            example: dad9dedb-9781-4ba1-842a-ea999e7479e1
      responses:
        '200':
          description: User succesfully removed from the organisation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/APIResponse"
              example:
                message: Succesfully removed user from organisation
        '401':
          $ref: "#/components/responses/Unauthorized"

  /organisations/{id}/invites:
    post:
      tags:
        - organisations
      summary: Create an invite token
      description: |
        To invite a user into an organisation, someone who is already a member
        create an invite token. This invite can only be accepted by the user
        which it is meant for.
      parameters:
        - $ref: "#/components/parameters/uuid"
      requestBody:
        required: true
        content:
          application/json:
            schema:
              properties:
                user:
                  type: string
                  format: UUID
                  example: dad9dedb-9781-4ba1-842a-ea999e7479e1
      responses:
        '201':
          description: Succesfully created invite link
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: Successfully created invite link
                  data:
                    type: string
                    example: "eyJhbGciOiJSUzI1Ni...lpxq2tOww"
        '404':
          description: Not found
          content:
            application/json:
              schema:
                $ref:  "#/components/schemas/APIError"
              examples:
                organisation_not_found:
                  value:
                    message: Organisation was not found
                    code: ORGANISATION_NOT_FOUND
                user_not_found:
                  value:
                    message: User was not found
                    code: USER_NOT_FOUND

  /organisations/{id}/invites/{token}:
    post:
      tags:
        - organisations
      summary: Accept invite token
      parameters:
        - $ref: "#/components/parameters/uuid"
        - name: token
          required: true
          in: path
          example: "eyJhbGciOiJSUzI1Ni...lpxq2tOww"
          schema:
            type: string
            format: JSON Web Token
      responses:
        '200':
          description: Succesfully joined organisation
          content:
            application/json:
              schema:
                properties:
                  message:
                    type: string
                    example: "Succesfully joined organisation"
        '400':
          description: Invalid invite token
          content:
            application/json:
              schema:
                $ref:  "#/components/schemas/APIError"
              examples:
                invalid:
                  description: Token is invalid
                  value:
                    message: Invalid invitation token
                    code: ORGANISATION_INVALID_INVITE_TOKEN
                expired:
                  description: Token is expired
                  value:
                    message: Expired invitation token
                    code: ORGANISATION_EXPIRED_INVITE_TOKEN
                already_member:
                    description: User is already a member of the organisation
                    value:
                      message: You are already a member of this organisation
                      code: ORGANISATION_USER_ALREADY_MEMBER

