CREATE TABLE organisations(
  id            SERIAL        PRIMARY KEY,
  name          VARCHAR(100)  NOT NULL  UNIQUE,
  address       VARCHAR(100)  NOT NULL,
  zipcode       VARCHAR(100)  NOT NULL,
  city          VARCHAR(100)  NOT NULL,
  archive_time  INTEGER       NOT NULL  DEFAULT(168),
  created_at    TIMESTAMP     NOT NULL DEFAULT(NOW())
);

CREATE TABLE users(
  id          SERIAL        PRIMARY KEY,
  email       VARCHAR(50)   NOT NULL,
  password    TEXT          NOT NULL,
  firstname   VARCHAR(20)   NOT NULL,
  lastname    VARCHAR(50)   NOT NULL,
  active      BOOLEAN       NOT NULL DEFAULT(FALSE),
  super_user   BOOLEAN       NOT NULL DEFAULT(FALSE),
  created_at  TIMESTAMP     NOT NULL DEFAULT(NOW())
);

CREATE TABLE organisation_user_members(
  user_id           SERIAL    NOT NULL,
  organisation_id   SERIAL    NOT NULL,

  -- The primary key is a combination of the user and the org ID
  PRIMARY KEY (user_id, organisation_id),

  -- Foreign key to the users. if the user is removed then also
  -- remove it from all organisations
  CONSTRAINT fk_user_member
    FOREIGN KEY(user_id)
      REFERENCES users(id)
      ON DELETE CASCADE,
  
  -- Foreign key to the organisation. if the organisation is removed
  -- then also remove all members
  CONSTRAINT fk_organisation_member
    FOREIGN KEY(organisation_id)
      REFERENCES organisations(id)
      ON DELETE CASCADE
);