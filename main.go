package main

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/internal/app"
	"gitlab.com/sensorbucket/identity/pkg/flagenv"
	"gitlab.com/sensorbucket/identity/pkg/web"
)

var (
	databaseURL    = flagenv.String("DB_URL", "dburl", "postgresql://localhost/identity", "Database connection string")
	httpAddress    = flagenv.String("ADDRESS", "address", "0.0.0.0", "Set the http listening address")
	httpPort       = flagenv.Int("PORT", "port", 3000, "Set the http listening port")
	privateKeyPath = flagenv.String("KEY_PATH", "keypath", "./private.key", "Use a local private key file instead of JWKS URI")
	jwksURI        = flagenv.String("JWKS_SIGNING", "jwks", "", "Set the uri to a private JSON Web Key Set")
	hostname       = flagenv.String("HOSTNAME", "hostname", "http://localhost:3000", "Set the address which the server is externally accessible from")
	appEnv         = flagenv.String("APP_ENV", "env", "development", "Set the app environment, either: development or production")
)

func main() {
	flag.Parse()

	// Initialize config
	config := &app.Config{
		IsProduction: *appEnv == "production",
		Hostname:     *hostname,
		HTTP: web.Config{
			Addr: *httpAddress,
			Port: *httpPort,
		},
		DatabaseURL:    *databaseURL,
		JWKSURI:        *jwksURI,
		PrivateKeyPath: *privateKeyPath,
	}

	// Configure logging
	config.Logger = &logrus.Logger{
		Out:       os.Stdout,
		Level:     logrus.DebugLevel,
		Formatter: new(logrus.TextFormatter),
	}
	if config.IsProduction {
		config.Logger.Level = logrus.WarnLevel
		config.Logger.Formatter = new(logrus.JSONFormatter)
	}

	// Start app
	app.Start(config)
}
