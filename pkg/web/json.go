package web

import (
	"encoding/json"
	"net/http"
)

// DecodeJSON ...
func DecodeJSON(r *http.Request, v interface{}) error {
	if r.Header.Get("content-type") != "application/json" {
		return ContentTypeError()
	}

	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		return InvalidJSONError()
	}
	return nil
}
