package web

import (
	"encoding/base64"
	"net/http"
	"net/url"
	"strconv"
)

// PageContext ...
type PageContext struct {
	Limit  int
	Offset int
	Cursor map[string]string
}

// PageMetadata ...
type PageMetadata struct {
	Next     string `json:"next"`
	Previous string `json:"previous"`
	Count    int    `json:"count"`
}

// Paginator is a configuration for a specific paginated endpoint
type Paginator struct {
	CursorFields []string
	DefaultLimit int
	MaxLimit     int
}

// PaginationError ...
func PaginationError() *APIError {
	return &APIError{
		Message:    "Invalid pagination",
		Code:       "PAGINATION_ERROR",
		HTTPStatus: http.StatusBadRequest,
	}
}

// ExtractFromRequest extracts relevant pagination information from the request
func (p *Paginator) ExtractFromRequest(r *http.Request) (*PageContext, error) {
	pctx := &PageContext{
		Limit:  p.DefaultLimit, // Default pagination limit
		Cursor: nil,            // No cursor by default
	}
	q := r.URL.Query()

	// Extract pagination limit
	qLim := q.Get("limit")
	if qLim != "" {
		lim, err := strconv.ParseInt(qLim, 10, 8)
		if err != nil || lim < 0 {
			return nil, PaginationError()
		}

		// Enforce max limit
		pctx.Limit = int(lim)
		if pctx.Limit > p.MaxLimit {
			pctx.Limit = p.MaxLimit
		}
	}

	// Extract page offset
	qPage := q.Get("page")
	if qPage != "" {
		page, err := strconv.ParseInt(qPage, 10, 8)
		if err != nil || page < 1 {
			return nil, PaginationError()
		}

		// The offset is based on the page times the limit
		pctx.Offset = pctx.Limit * (int(page) - 1)
	}

	// Extract cursor
	qCur := q.Get("cursor")
	if qCur != "" {
		fields, err := p.decodeCursor(qCur)
		if err != nil {
			return nil, PaginationError()
		}

		pctx.Cursor = fields
	}

	return pctx, nil
}

// decodeCursor decodes a base64 cursor to field/value format
func (p *Paginator) decodeCursor(cb64 string) (map[string]string, error) {
	fields := map[string]string{}
	// decode base64 to string
	cb, err := base64.RawURLEncoding.DecodeString(cb64)
	if err != nil {
		return nil, err
	}
	cur := string(cb)

	vals, err := url.ParseQuery(cur)
	if err != nil {
		return nil, err
	}

	// Ensure fields are allowed
	for field := range vals {
		if !includes(p.CursorFields, field) {
			return nil, PaginationError()
		}
		if len(vals[field]) > 1 {
			return nil, PaginationError()
		}

		fields[field] = vals[field][0]
	}

	return fields, nil
}

func includes(arr []string, i string) bool {
	for _, s := range arr {
		if s == i {
			return true
		}
	}
	return false
}
