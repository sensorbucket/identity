package web

import "net/http"

type OrderDirection string

var (
	OrderAscending  OrderDirection = "ASC"
	OrderDescending OrderDirection = "DESC"
)

// Ordering contains ordering information
type Ordering struct {
	By  string
	Dir OrderDirection
}

// NewQueryOrderer creates a function to extract ordering information from
// an http request
func NewQueryOrderer(fields []string) func(r *http.Request) *Ordering {
	return func(r *http.Request) *Ordering {
		o := &Ordering{
			"",
			"ASC",
		}

		qBy := r.URL.Query().Get("order_by")
		if qBy == "" || !includes(fields, qBy) {
			return nil
		}
		o.By = qBy

		qDir := r.URL.Query().Get("order_direction")
		if qDir != "" && (qDir == "ASC" || qDir == "DESC") {
			o.Dir = OrderDirection(qDir)
		}

		return o
	}
}
