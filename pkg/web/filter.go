package web

import "net/http"

// NewFilter Creates a new Filter
func NewFilter(fields []string) func(*http.Request) map[string]interface{} {
	return func(r *http.Request) map[string]interface{} {
		m := map[string]interface{}{}
		for _, f := range fields {
			v := r.URL.Query().Get(f)
			if v != "" {
				m[f] = v
			}
		}
		return m
	}
}
