package web

// ContentTypeError ...
func ContentTypeError() error {
	return &APIError{
		HTTPStatus: 400,
		Message:    "Invalid content type",
		Code:       "INVALID_CONTENT_TYPE",
	}
}

// InvalidJSONError ...
func InvalidJSONError() error {
	return &APIError{
		HTTPStatus: 400,
		Message:    "Malformed JSON",
		Code:       "MALFORMED_JSON",
	}
}

// ValidationError ...
func ValidationError() error {
	return &APIError{
		HTTPStatus: 400,
		Message:    "Validation error",
		Code:       "VALIDATION_ERROR",
	}
}

// ForbiddenError ...
func ForbiddenError() error {
	return &APIError{
		HTTPStatus: 401,
		Message:    "Not authorized to perform this action",
		Code:       "FORBIDDEN",
	}
}
