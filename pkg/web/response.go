package web

import (
	"encoding/json"
	"net/http"

	logger "github.com/sirupsen/logrus"
)

// APIError is an API consumer friendly error
type APIError struct {
	Message    string `json:"message,omitempty"`
	Code       string `json:"code,omitempty"`
	HTTPStatus int    `json:"-"`
}

// APIResponse ...
type APIResponse struct {
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

// PaginatedAPIResponse is a generic response with pagination metadata embedded
type PaginatedAPIResponse struct {
	Message    string        `json:"message,omitempty"`
	Data       interface{}   `json:"data,omitempty"`
	Pagination *PageMetadata `json:"pagination,omitempty"`
}

func (e *APIError) Error() string {
	return e.Message
}

// HTTPResponse ...
func HTTPResponse(w http.ResponseWriter, s int, r interface{}) {
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(s)
	json.NewEncoder(w).Encode(r)
}

// HTTPError writes the error to a http response writer
func HTTPError(w http.ResponseWriter, e interface{}) {
	w.Header().Set("content-type", "application/json")
	switch err := e.(type) {
	case *APIError:
		w.WriteHeader(err.HTTPStatus)
		json.NewEncoder(w).Encode(err)
	default:
		logger.Errorf("non APIError occured: %s", err)
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": "Internal server error",
		})
	}
}
