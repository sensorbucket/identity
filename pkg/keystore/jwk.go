package keystore

/*
	Most of this code is directly copied and modified from:
		https://github.com/square/go-jose
*/

import (
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"math/big"
)

type byteBuffer struct {
	data []byte
}

// UnmarshalJSON unmarshals JWK data to a byte array
func (b *byteBuffer) UnmarshalJSON(data []byte) error {
	var b64 string
	err := json.Unmarshal(data, &b64)
	if err != nil {
		return err
	}
	if b64 == "" {
		return nil
	}

	decoded, err := base64.RawURLEncoding.DecodeString(b64)
	if err != nil {
		return err
	}

	b.data = decoded
	return nil
}

func (b byteBuffer) bigInt() *big.Int {
	return new(big.Int).SetBytes(b.data)
}

func (b byteBuffer) toInt() int {
	return int(b.bigInt().Int64())
}

// rawJSONWebKey is the json format of a jwk
type rawJSONWebKey struct {
	Kty string `json:"kty,omitempty"`
	Kid string `json:"kid,omitempty"`
	Alg string `json:"alg,omitempty"`
	Use string `json:"use,omitempty"`

	N *byteBuffer `json:"n,omitempty"`
	E *byteBuffer `json:"e,omitempty"`

	// -- Following fields are only used for private keys --
	// RSA uses D, P and Q, while ECDSA uses only D. Fields Dp, Dq, and Qi are
	// completely optional. Therefore for RSA/ECDSA, D != nil is a contract that
	// we have a private key whereas D == nil means we have only a public key.
	D  *byteBuffer `json:"d,omitempty"`
	P  *byteBuffer `json:"p,omitempty"`
	Q  *byteBuffer `json:"q,omitempty"`
	Dp *byteBuffer `json:"dp,omitempty"`
	Dq *byteBuffer `json:"dq,omitempty"`
	Qi *byteBuffer `json:"qi,omitempty"`
}

// JSONWebKey is the parsed key with information
type JSONWebKey struct {
	Key       interface{}
	KeyID     string
	Algorithm string
	Use       string
}

// JSONWebKeySet is a set of JSONWebKeys
type JSONWebKeySet struct {
	Keys []JSONWebKey `json:"keys"`
}

// UnmarshalJSON unmarshals a key from its JSON representation into a parsed
// key
func (k *JSONWebKey) UnmarshalJSON(data []byte) error {
	var raw rawJSONWebKey
	err := json.Unmarshal(data, &raw)
	if err != nil {
		return err
	}

	// Parse RSA keys
	if raw.Kty == "RSA" {
		*k = JSONWebKey{
			KeyID:     raw.Kid,
			Algorithm: raw.Alg,
			Use:       raw.Use,
		}

		// D is only present on private keys
		if raw.D != nil {
			rsaKey, err := raw.toRSAPrivateKey()
			if err != nil {
				return err
			}
			k.Key = rsaKey
		} else {
			rsaKey, err := raw.toRSAPublicKey()
			if err != nil {
				return err
			}
			k.Key = rsaKey
		}

		return nil
	}

	return errors.New("keystore only support RSA keys for now")
}

func (r *rawJSONWebKey) toRSAPrivateKey() (*rsa.PrivateKey, error) {
	if r.D == nil || r.P == nil || r.Q == nil {
		return nil, errors.New("malformed key, missing D, P or Q")
	}
	if r.N == nil {
		return nil, errors.New("malformed key, missing N")
	}
	if r.E == nil {
		return nil, errors.New("malformed key, missing E")
	}
	if r.D == nil {
		return nil, errors.New("malformed key, missing D")
	}
	if r.P == nil {
		return nil, errors.New("malformed key, missing P")
	}
	if r.Q == nil {
		return nil, errors.New("malformed key, missing Q")
	}

	return &rsa.PrivateKey{
		PublicKey: rsa.PublicKey{
			N: r.N.bigInt(),
			E: r.E.toInt(),
		},
		D: r.D.bigInt(),
		Primes: []*big.Int{
			r.P.bigInt(),
			r.Q.bigInt(),
		},
	}, nil
}

func (r *rawJSONWebKey) toRSAPublicKey() (*rsa.PublicKey, error) {
	if r.N == nil {
		return nil, errors.New("malformed key, missing N")
	}
	if r.E == nil {
		return nil, errors.New("malformed key, missing E")
	}

	return &rsa.PublicKey{
		N: r.N.bigInt(),
		E: r.E.toInt(),
	}, nil
}
