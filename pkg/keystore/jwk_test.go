package keystore_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/sensorbucket/identity/pkg/keystore"
)

func TestUnmarshalJWK(t *testing.T) {
	pk := mkPrivateKey()
	data := `{
		"kty": "RSA",
		"n": "mKjJEE4OhIo8RD3ZA-u2838Mz23kcqoU6j37q5IZIY4yuvVAuquugxH2cr1tEPEA9zgUys0-WPAu0_UwzWv-Yw",
		"e": "AQAB",
		"d": "XGfevSXil8vtSwl88WifJ6lCIdVNMTNO0bOPQX2ABNTU_tVY_pacR58zKy1SHJiMkb1-KHBhuuNJdrU9O6Es0Q",
		"p": "xExPKHJm3Co2BUM3aO2d6JMjh6QYPC1TXlPDQghsthc",
		"q": "xxbHf3hSe-kyFnhTZsuvck76UCsktoRtqjHCAhJu9ZU",
		"dp": "b9_p0Wx6EQqBpTgs0UFzmMSbIEg2g711YTIt-2lfw9s",
		"dq": "lHLc0KSERJauXWjdL2IjcUWIieyRlHKMXwJ5Ghhqmj0",
		"qi": "DIr3wCT2pEdBM1kPfU9bJMKeMjZVmfJdBRM17W_dPD4",
		"alg": "RS256",
		"kid": "abcdef",
		"use": "sig"
	}`

	jwk := &keystore.JSONWebKey{}
	err := json.Unmarshal([]byte(data), jwk)
	if err != nil {
		t.Errorf("could not unmarshal JWK: %v", err)
		return
	}

	// Check if keys are the considered the same
	if !pk.Equal(jwk.Key) {
		t.Error("kecoded private key does not equal original private key")
	}

	// Check if kid, alg and use are correctly interpreted
	if jwk.KeyID != "abcdef" {
		t.Error("JWK does not contain correct KID")
	}

	if jwk.Algorithm != "RS256" {
		t.Error("JWK does not contain correct Algorithm")
	}

	if jwk.Use != "sig" {
		t.Error("JWK does not contain correct Usage")
	}
}

func TestUnmarshalJWKS(t *testing.T) {
	data := `{
		"keys": [
			{
				"kty": "RSA",
				"n": "mKjJEE4OhIo8RD3ZA-u2838Mz23kcqoU6j37q5IZIY4yuvVAuquugxH2cr1tEPEA9zgUys0-WPAu0_UwzWv-Yw",
				"e": "AQAB",
				"d": "XGfevSXil8vtSwl88WifJ6lCIdVNMTNO0bOPQX2ABNTU_tVY_pacR58zKy1SHJiMkb1-KHBhuuNJdrU9O6Es0Q",
				"p": "xExPKHJm3Co2BUM3aO2d6JMjh6QYPC1TXlPDQghsthc",
				"q": "xxbHf3hSe-kyFnhTZsuvck76UCsktoRtqjHCAhJu9ZU",
				"dp": "b9_p0Wx6EQqBpTgs0UFzmMSbIEg2g711YTIt-2lfw9s",
				"dq": "lHLc0KSERJauXWjdL2IjcUWIieyRlHKMXwJ5Ghhqmj0",
				"qi": "DIr3wCT2pEdBM1kPfU9bJMKeMjZVmfJdBRM17W_dPD4",
				"alg": "RS256",
				"kid": "keyid1",
				"use": "sig"
			},
			{
				"kty": "RSA",
				"n": "mKjJEE4OhIo8RD3ZA-u2838Mz23kcqoU6j37q5IZIY4yuvVAuquugxH2cr1tEPEA9zgUys0-WPAu0_UwzWv-Yw",
				"e": "AQAB",
				"d": "XGfevSXil8vtSwl88WifJ6lCIdVNMTNO0bOPQX2ABNTU_tVY_pacR58zKy1SHJiMkb1-KHBhuuNJdrU9O6Es0Q",
				"p": "xExPKHJm3Co2BUM3aO2d6JMjh6QYPC1TXlPDQghsthc",
				"q": "xxbHf3hSe-kyFnhTZsuvck76UCsktoRtqjHCAhJu9ZU",
				"dp": "b9_p0Wx6EQqBpTgs0UFzmMSbIEg2g711YTIt-2lfw9s",
				"dq": "lHLc0KSERJauXWjdL2IjcUWIieyRlHKMXwJ5Ghhqmj0",
				"qi": "DIr3wCT2pEdBM1kPfU9bJMKeMjZVmfJdBRM17W_dPD4",
				"alg": "RS256",
				"kid": "keyid2",
				"use": "sig"
			},
			{
				"kty": "RSA",
				"n": "mKjJEE4OhIo8RD3ZA-u2838Mz23kcqoU6j37q5IZIY4yuvVAuquugxH2cr1tEPEA9zgUys0-WPAu0_UwzWv-Yw",
				"e": "AQAB",
				"d": "XGfevSXil8vtSwl88WifJ6lCIdVNMTNO0bOPQX2ABNTU_tVY_pacR58zKy1SHJiMkb1-KHBhuuNJdrU9O6Es0Q",
				"p": "xExPKHJm3Co2BUM3aO2d6JMjh6QYPC1TXlPDQghsthc",
				"q": "xxbHf3hSe-kyFnhTZsuvck76UCsktoRtqjHCAhJu9ZU",
				"dp": "b9_p0Wx6EQqBpTgs0UFzmMSbIEg2g711YTIt-2lfw9s",
				"dq": "lHLc0KSERJauXWjdL2IjcUWIieyRlHKMXwJ5Ghhqmj0",
				"qi": "DIr3wCT2pEdBM1kPfU9bJMKeMjZVmfJdBRM17W_dPD4",
				"alg": "RS256",
				"kid": "keyid3",
				"use": "sig"
			}
		]
	}`

	jwks := &keystore.JSONWebKeySet{}
	err := json.Unmarshal([]byte(data), jwks)
	if err != nil {
		t.Errorf("could not unmarshal JWKS: %v", err)
		return
	}

	if len(jwks.Keys) < 3 {
		t.Errorf("not all keys were decoded")
	}

	// Preserve ordering
	if jwks.Keys[0].KeyID != "keyid1" ||
		jwks.Keys[1].KeyID != "keyid2" ||
		jwks.Keys[2].KeyID != "keyid3" {
		t.Errorf("order of keys was not preserved")
	}
}
