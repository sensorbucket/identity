package keystore

// StaticKeystore a static keystore is a keystore that simply holds keys with ids
type StaticKeystore struct {
	latest string
	keymap map[string]*Key
}

// NewStaticKeystore Creates a new static keystore
func NewStaticKeystore() *StaticKeystore {
	return &StaticKeystore{
		latest: "",
		// keymap must be initialized or methods will throw an error
		keymap: map[string]*Key{},
	}
}

// Set a key in the store
// to also set is as the latest use `#Add`
func (k *StaticKeystore) Set(kid string, key interface{}) {
	k.keymap[kid] = &Key{
		Content: key,
		ID:      kid,
	}
}

// Add will add a key and assume it is the newest key available
// to "add" a key but not have it be set as the latest use `#set`
func (k *StaticKeystore) Add(kid string, key interface{}) {
	k.Set(kid, key)
	k.latest = kid
}

// =======================================================
//  Keystore interface implementation
// =======================================================

// Get returns a JSON Web Key with the given key ID
func (k *StaticKeystore) Get(kid string) *Key {
	return k.keymap[kid]
}

// GetLatest returns the latest key
func (k *StaticKeystore) GetLatest() *Key {
	return k.Get(k.latest)
}
