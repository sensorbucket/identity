package keystore

import "crypto/rsa"

// Keyprovider provides private and/or public keys
type Keyprovider interface {
	GetLatest() *Key
	Get(kid string) *Key
}

// Key represents a key
type Key struct {
	Content interface{}
	ID      string
}

// RSAPublicKey returns the public key if it exists or nil
func (k *Key) RSAPublicKey() *rsa.PublicKey {
	switch v := k.Content.(type) {
	case *rsa.PublicKey:
		return v
	case *rsa.PrivateKey:
		return &v.PublicKey
	}
	return nil
}

// RSAPrivateKey returns the private key if it exists or nil
func (k *Key) RSAPrivateKey() *rsa.PrivateKey {
	pubkey, ok := k.Content.(*rsa.PrivateKey)
	if !ok {
		return nil
	}
	return pubkey
}
