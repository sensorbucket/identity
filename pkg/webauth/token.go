package webauth

import (
	"fmt"
	"strconv"

	"github.com/dgrijalva/jwt-go"
)

// Token ...
type Token interface {
	CanAccessOrganisation(id int) bool
}

type TokenValidator interface {
	Validate() error
}

// UserToken ...
type UserToken struct {
	jwt.StandardClaims
	OrganisationIDs []int `json:"orgs,omitempty"`
	SuperUser       *bool `json:"su,omitempty"`
	UserID          int   `json:"-"`
}

func (t *UserToken) CanAccessOrganisation(id int) bool {
	// If the token is a superuser than it has access to everything
	if t.SuperUser != nil && *t.SuperUser == true {
		return true
	}

	// Check if organisation id exists in the organisation array in the token
	for ix := 0; ix < len(t.OrganisationIDs); ix++ {
		if t.OrganisationIDs[ix] == id {
			return true
		}
	}

	// Any other case has no access
	return false
}

// Validates the userToken
func (t *UserToken) Validate() error {
	id, err := strconv.ParseInt(t.Subject, 10, 32)
	if err != nil {
		return fmt.Errorf("token subject is not an integer: %w", err)
	}

	t.UserID = int(id)
	return nil
}

// SourceToken ...
type SourceToken struct {
	jwt.StandardClaims
	OrganisationID int `json:"org,omitempty"`
}

func (t *SourceToken) CanAccessOrganisation(id int) bool {
	return id == t.OrganisationID
}

// ServiceToken ...
type ServiceToken struct {
	jwt.StandardClaims
}

func (t *ServiceToken) CanAccessOrganisation(id int) bool {
	return true
}
