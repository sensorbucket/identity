package webauth

import (
	"net/http"

	"gitlab.com/sensorbucket/identity/pkg/web"
)

// UnauthorizedError appears when no login credentials were provided
func UnauthorizedError() *web.APIError {
	return &web.APIError{
		Message:    "Authentication required",
		Code:       "UNAUTHORIZED",
		HTTPStatus: http.StatusUnauthorized,
	}
}

// InvalidAuthError appears when incorrect login credentials were provided
func InvalidAuthError() *web.APIError {
	return &web.APIError{
		Message:    "Authentication failed",
		Code:       "AUTH_INVALID",
		HTTPStatus: http.StatusForbidden,
	}
}

// AuthTokenExpiredError appears when the token has expired
func AuthTokenExpiredError() *web.APIError {
	return &web.APIError{
		Message:    "Authentication token has expired",
		Code:       "AUTH_EXPIRED",
		HTTPStatus: http.StatusForbidden,
	}
}
