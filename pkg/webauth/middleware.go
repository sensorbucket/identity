package webauth

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/identity/pkg/web"
)

var CTXTokenKey = struct{}{}

// AuthClaims ...
type AuthClaims struct {
	jwt.StandardClaims
}

// Create creates middleware suitable for the given key
func Create(ks keystore.Keyprovider) func(next http.Handler) http.Handler {
	logger := logrus.WithField("pkg", "webauth.middleware")
	return func(next http.Handler) http.Handler {
		mw := func(rw http.ResponseWriter, r *http.Request) {
			// Get authorization header
			hdr := r.Header.Get("authorization")
			if hdr == "" {
				logger.Warn("Authorization failed because not authorization was present")
				web.HTTPError(rw, UnauthorizedError())
				return
			}

			parts := strings.Split(hdr, " ")
			if len(parts) != 2 {
				logger.Warn("Authorization failed because authorization header does not have two parts")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			if strings.ToLower(parts[0]) != "bearer" {
				logger.Warn("Authorization failed because authorization header is not of bearer type")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			// Validate token parts
			tStr := parts[1]
			tParts := strings.Split(tStr, ".")
			if len(tParts) != 3 {
				logger.Warn("Authorization failed because jwt has no three parts")
				web.HTTPError(rw, InvalidAuthError())
				return
			}
			// tHeader := tParts[0]
			tPayloadB64 := tParts[1]
			// tSignature := tParts[2]

			// decode base64
			tPayload, err := base64.RawStdEncoding.DecodeString(tPayloadB64)
			if err != nil {
				logger.WithError(err).Warn("Authorization failed because jwt payload could not be base64 decoded")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			// Unmarshal payload
			var stdClaims = &jwt.StandardClaims{}
			err = json.Unmarshal([]byte(tPayload), &stdClaims)
			if err != nil {
				logger.WithError(err).Warn("Authorization failed because jwt payload are not unmarshallable")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			// A type of token implements the claims interface, so when you see claims
			// think UserToken, SourceToken, ServiceToken etc. This switch figures out
			// which kind of token it is. The `claims` variable is unmarshalled to by
			// the `ParseWithClaims` a bit further on
			var claims jwt.Claims
			switch stdClaims.Audience {
			case "user":
				claims = &UserToken{}
			case "source":
				claims = &SourceToken{}
			case "service":
				claims = &ServiceToken{}
			default:
				logger.WithField("aud", stdClaims.Audience).Warn("Authorization failed because JWT audience is invalid")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			// Get token string
			t, err := jwt.ParseWithClaims(tStr, claims, func(t *jwt.Token) (interface{}, error) {
				// Make sure the token is signed with RSA key
				if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
					logger.Warn("Authorization failed, JWT has invalid signing method")
					return nil, InvalidAuthError()
				}

				// Get the id of the key with which the token was signed
				kid, ok := t.Header["kid"].(string)
				if !ok {
					logger.Warn("Authorization failed, JWT has no KID claim")
					return nil, InvalidAuthError()
				}

				// Fetch the correct token, nil if it does not exit
				key := ks.Get(kid)
				if key == nil {
					logger.Warn("Authorization failed, key for JWT KID could not be found")
					return nil, InvalidAuthError()
				}

				// Get the public key from the generic key
				// returns nil if the key does not contain an RSA public key
				pubkey := key.RSAPublicKey()
				if pubkey == nil {
					logger.Warn("Authorization failed, key with JWT KID is not an RSA key")
					return nil, InvalidAuthError()
				}

				return pubkey, nil
			})
			if err != nil {
				// Cast to a JWT ValidationError and check specifically for an
				// expiration error, so that the correct error can be returned
				if verr, ok := err.(*jwt.ValidationError); ok {
					if (verr.Errors & jwt.ValidationErrorExpired) != 0 {
						logger.Warn("Authorization failed, JWT is expired")
						web.HTTPError(rw, AuthTokenExpiredError())
						return
					}
				}
				logger.WithError(err).Warn("Authorization failed, JWT is invalid")
				web.HTTPError(rw, InvalidAuthError())
				return
			}

			// Check if token must be validated
			validateToken, isValidatable := t.Claims.(TokenValidator)
			if isValidatable {
				err = validateToken.Validate()
				if err != nil {
					logger.Warn("Authorization failed, JWT to Token could not be validated")
					web.HTTPError(rw, InvalidAuthError())
					return
				}
			}

			// Add claims to CTXTokenKey
			ctx := context.WithValue(
				r.Context(),
				CTXTokenKey,
				t.Claims,
			)
			next.ServeHTTP(rw, r.WithContext(ctx))
		}
		return http.HandlerFunc(mw)
	}
}
