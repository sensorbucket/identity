module gitlab.com/sensorbucket/identity

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/doug-martin/goqu/v9 v9.10.0
	github.com/go-chi/chi v1.5.1
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.4
)
