package authmod

import (
	"net/http"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/sensorbucket/identity/pkg/web"
)

func (m *Module) login() http.HandlerFunc {
	// DTO contains the required body properties
	type DTO struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	// validate is a function to validate whether the DTO is correct
	validate := func(dto *DTO) error {
		return validation.ValidateStruct(
			dto,
			validation.Field(
				&dto.Email,
				validation.Required,
				is.Email,
			),
			validation.Field(
				&dto.Password,
				validation.Required,
			),
		)
	}

	// This is the actual request handler
	return func(rw http.ResponseWriter, r *http.Request) {
		var dto DTO

		// Decode the DTO from the body
		err := web.DecodeJSON(r, &dto)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Validate the DTO
		err = validate(&dto)
		if err != nil {
			web.HTTPError(rw, web.ValidationError())
			return
		}

		// Try to authenticate the user
		u, err := m.Authenticate(dto.Email, dto.Password)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Create a token for the user
		t, err := m.CreateUserToken(u)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Respond
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully logged in",
			Data:    t,
		})
	}
}

func (m *Module) register() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Write([]byte("Hello from auth register"))
	}
}

// Routes creates router for the auth routes
func (m *Module) Routes() *chi.Mux {
	r := chi.NewRouter()

	r.Post("/login", m.login())
	r.Post("/register", m.register())

	return r
}
