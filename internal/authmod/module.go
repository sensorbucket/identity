package authmod

import (
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/internal/orgmod"
	"gitlab.com/sensorbucket/identity/internal/usermod"
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/identity/pkg/webauth"
)

var (
	tokenAudience = "user"
	tokenLifetime = 1e9 * 60 * 60 * 24 // 1 day in nanoseconds
)

// Module contains dependencies for the auth module
type Module struct {
	userm    *usermod.Module
	orgm     *orgmod.Module
	keystore keystore.Keyprovider
	logger   *logrus.Entry
}

// Config contains dependencies for this module
type Config struct {
	UserModule  *usermod.Module
	OrgModule   *orgmod.Module
	KeyProvider keystore.Keyprovider
	Logger      *logrus.Logger
}

// New creates a new authentication module
func New(c *Config) *Module {
	return &Module{
		userm:    c.UserModule,
		orgm:     c.OrgModule,
		keystore: c.KeyProvider,
		logger:   c.Logger.WithField("module", "authmod"),
	}
}

// Authenticate Try to authenticate a user through its email and password
func (m *Module) Authenticate(email string, password string) (*usermod.User, error) {
	u, err := m.userm.GetByEmail(email)
	if err != nil {
		m.logger.WithField("email", email).WithError(err).Error("User authentication failed, could not get user by email")
		return nil, err
	}

	err = u.ComparePassword(password)
	if err != nil {
		m.logger.WithField("email", email).WithError(err).Errorf("User authentication failed, password compare failed")
		return nil, err
	}

	return u, nil
}

// CreateUserToken creates a token that can be used to authenticate the given
// user
func (m *Module) CreateUserToken(u *usermod.User) (string, error) {
	// Get latest private key
	key := m.keystore.GetLatest()
	if key == nil {
		err := errors.New("could not create user token because there is no key available in the keystore")
		m.logger.WithField("user_id", u.ID).WithError(err).Error()
		return "", err
	}
	privkey := key.RSAPrivateKey()
	if privkey == nil {
		err := errors.New("could not create user token because the latest key in the keystore is not an RSA key")
		m.logger.WithField("user_id", u.ID).WithField("key_id", key.ID).WithError(err).Error()
		return "", err
	}

	// Add token information
	orgs, err := m.orgm.GetOrganisationsForUser(u.ID)
	if err != nil {
		return "", err
	}
	orgIDs := make([]int, len(orgs))
	for ix := 0; ix < len(orgs); ix++ {
		orgIDs[ix] = orgs[ix].ID
	}

	// Create the correct claims for this token and then create and sign the
	// JSON Web Token using the private key from the passed config
	claims := &webauth.UserToken{
		StandardClaims: jwt.StandardClaims{
			Audience:  tokenAudience,
			Subject:   fmt.Sprint(u.ID),
			ExpiresAt: time.Now().Add(time.Duration(tokenLifetime)).Unix(),
		},
		OrganisationIDs: orgIDs,
	}
	// The "su" claim should not be added to the claims if the user is not a
	// superuser, but if we set it to `false` it will still be serialized. So
	// instead we use a pointer which can be "nil" and wont be serialized.
	if u.SuperUser {
		claims.SuperUser = &u.SuperUser
	}

	m.logger.WithFields(logrus.Fields{
		"user":   u.SuperUser,
		"claims": claims.SuperUser,
	}).Info("IS SUPER")

	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	// Add the key id with which the token was signed to the token header
	t.Header["kid"] = key.ID

	// Sign the token
	tStr, err := t.SignedString(privkey)
	if err != nil {
		return "", err
	}
	return tStr, nil
}
