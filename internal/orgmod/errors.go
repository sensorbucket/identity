package orgmod

import "gitlab.com/sensorbucket/identity/pkg/web"

var (
	// DuplicateNameError ...
	DuplicateNameError = &web.APIError{
		Message:    "An organisation with this name already exists",
		Code:       "ORGANISATION_DUPLICATE_NAME",
		HTTPStatus: 400,
	}

	// NotFoundError ...
	NotFoundError = &web.APIError{
		Message:    "Organisation was not found",
		Code:       "ORGANISATION_NOT_FOUND",
		HTTPStatus: 404,
	}

	ErrInvalidTokenType = &web.APIError{
		Message:    "Token is not meant for this resource",
		Code:       "ORGANISATION_INVALID_AUTH_TOKEN",
		HTTPStatus: 401,
	}

	// InvalidInviteTokenError ...
	InvalidInviteTokenError = &web.APIError{
		Message:    "Invalid invitation token",
		Code:       "ORGANISATION_INVALID_INVITE_TOKEN",
		HTTPStatus: 400,
	}

	// ExpiredInviteTokenError ...
	ExpiredInviteTokenError = &web.APIError{
		Message:    "Expired invitation token",
		Code:       "ORGANISATION_EXPIRED_INVITE_TOKEN",
		HTTPStatus: 400,
	}

	// AlreadyAMemberError ...
	AlreadyAMemberError = &web.APIError{
		Message:    "You are already a member of this organisation",
		Code:       "ORGANISATION_USER_ALREADY_MEMBER",
		HTTPStatus: 400,
	}
)
