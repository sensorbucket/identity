package orgmod

import (
	"strconv"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	// Imports the postgres dialect for goqu
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"

	"gitlab.com/sensorbucket/identity/internal/usermod"
	"gitlab.com/sensorbucket/identity/pkg/web"
)

// Store represents the required methods for a repository
type Store interface {
	FindByID(id int) (*Organisation, error)
	FindByName(n string) (*Organisation, error)
	List(pctx *web.PageContext, filter map[string]interface{}, ordering *web.Ordering) ([]*Organisation, *web.PageMetadata, error)
	Create(o *Organisation) (*Organisation, error)
	CreateWithOwner(o *Organisation, uID int) (*Organisation, error)
	AddMember(m *OrganisationUserMember) error
	RemoveMember(oID int, uID int) error
	HasMember(oID int, uID int) (bool, error)
	GetMembers(oID int) ([]*usermod.User, error)
	GetOrganisationsForUser(uID int) ([]*Organisation, error)
}

// GormStore represents a Postgres database store
type GormStore struct {
	db *gorm.DB
}

// NewGormStore ...
func NewGormStore(db *gorm.DB) *GormStore {
	return &GormStore{
		db: db,
	}
}

// FindByID ...
func (s *GormStore) FindByID(id int) (*Organisation, error) {
	var org Organisation
	res := s.db.Find(&org, id)

	if res.RowsAffected == 0 {
		return nil, nil
	}
	return &org, res.Error
}

// FindByName ...
func (s *GormStore) FindByName(name string) (*Organisation, error) {
	var org Organisation
	res := s.db.Find(&org, "name = ?", name)

	if res.RowsAffected == 0 {
		return nil, nil
	}
	return &org, res.Error
}

// List ...
func (s *GormStore) List(pctx *web.PageContext, filter map[string]interface{}, ordering *web.Ordering) ([]*Organisation, *web.PageMetadata, error) {
	orgs := make([]*Organisation, 0)
	stmt := s.db.Model(&Organisation{})

	// Apply filters
	stmt.Where(filter)

	// Paging limit
	stmt.Limit(pctx.Limit)
	// Paging cursor
	if v, ok := pctx.Cursor["offset"]; ok {
		offset, err := strconv.ParseInt(v, 10, 32)
		if err != nil {
			return nil, nil, err
		}
		stmt.Offset(int(offset))
	}

	// Ordering
	if ordering != nil {
		stmt.Order(clause.OrderByColumn{
			Column: clause.Column{Name: ordering.By},
			Desc:   ordering.Dir == web.OrderDescending,
		})
	}

	res := stmt.Scan(&orgs)

	pageMeta := &web.PageMetadata{
		Count: int(res.RowsAffected),
	}

	return orgs, pageMeta, res.Error
}

// Create ...
func (s *GormStore) Create(org *Organisation) (*Organisation, error) {
	res := s.db.Omit("id").Create(org)

	return org, res.Error
}

// CreateWithOwner creates an organisation and directly adds an owner
func (s *GormStore) CreateWithOwner(org *Organisation, uID int) (*Organisation, error) {
	var err error
	err = s.db.Transaction(func(tx *gorm.DB) error {
		txs := NewGormStore(tx)

		// Create organisation
		org, err = txs.Create(org)
		if err != nil {
			return err
		}

		// Add member
		err = txs.AddMember(&OrganisationUserMember{
			OrganisationID: org.ID,
			UserID:         uID,
		})
		return err
	})

	return org, err
}

// AddMember adds a user to an organisation as member
func (s *GormStore) AddMember(m *OrganisationUserMember) error {
	res := s.db.Create(m)
	return res.Error
}

// RemoveMember removes a user from an organisation as member
func (s *GormStore) RemoveMember(oID int, uID int) error {
	res := s.db.Delete(&OrganisationUserMember{
		OrganisationID: oID,
		UserID:         uID,
	})
	return res.Error
}

// HasMember checks whether the given organisation has a user as member
func (s *GormStore) HasMember(oID int, uID int) (bool, error) {
	var cnt int64
	res := s.db.Model(&OrganisationUserMember{}).Where("organisation_id = ? AND user_id = ?", oID, uID).Count(&cnt)
	return cnt > 0, res.Error
}

// GetMembers retrieves all users that belong to the given organisation
func (s *GormStore) GetMembers(oID int) ([]*usermod.User, error) {
	members := make([]*usermod.User, 0)
	res := s.db.Model(&usermod.User{}).
		Joins("left join organisation_user_members as uom on users.id = uom.user_id").
		Where("uom.organisation_id = ?", oID).
		Scan(&members)

	return members, res.Error
}

// GetOrganisationsForUser retrieves all organisations that belong a user belongs to
func (s *GormStore) GetOrganisationsForUser(uID int) ([]*Organisation, error) {
	orgs := make([]*Organisation, 0)
	res := s.db.Model(&Organisation{}).
		Joins("left join organisation_user_members as uom on organisations.id = uom.organisation_id").
		Where("uom.user_id = ?", uID).
		Scan(&orgs)

	return orgs, res.Error
}
