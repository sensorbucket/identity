package orgmod

import (
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gitlab.com/sensorbucket/identity/pkg/webauth"
)

func (m *Module) httpGetOne() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		_id := chi.URLParam(r, "orgid")
		id, err := strconv.ParseInt(_id, 10, 32)

		o, err := m.GetByID(int(id))
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully retrieved organisation",
			Data:    o,
		})
	}
}

func (m *Module) httpGetMany() http.HandlerFunc {
	// Initialize the paginator for this endpoint
	pag := &web.Paginator{
		CursorFields: []string{"offset"},
		DefaultLimit: 10,
		MaxLimit:     20,
	}

	// Initialize the query filter for this endpoint
	// these values are used to filter out results
	fltr := web.NewFilter([]string{"name", "city"})

	// Initialize ordering information extraction function
	order := web.NewQueryOrderer([]string{"name", "created_at", "archiveTime", "zipcode", "address"})

	return func(rw http.ResponseWriter, r *http.Request) {
		// Extract pagination information
		pctx, err := pag.ExtractFromRequest(r)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Extract filter query
		fctx := fltr(r)

		// Extract ordering
		octx := order(r)

		orgs, page, err := m.GetPage(pctx, fctx, octx)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Respond
		response := web.PaginatedAPIResponse{
			Message:    "Successfully fetched organisations",
			Data:       orgs,
			Pagination: page,
		}
		web.HTTPResponse(rw, http.StatusOK, response)
	}
}

func (m *Module) httpCreateOne() http.HandlerFunc {
	// DTO contains the data required for this function to perform
	type DTO struct {
		Name    string `json:"name"`
		Address string `json:"address"`
		Zipcode string `json:"zipcode"`
		City    string `json:"city"`
	}

	// validate the DTO
	validate := func(dto *DTO) error {
		return validation.ValidateStruct(
			dto,
			validation.Field(&dto.Name, validation.Required),
			validation.Field(&dto.Address, validation.Required),
			validation.Field(&dto.Zipcode, validation.Required),
			validation.Field(&dto.City, validation.Required),
		)
	}

	return func(rw http.ResponseWriter, r *http.Request) {
		// Get and validate the DTO from the request
		var dto DTO
		err := web.DecodeJSON(r, &dto)
		if err != nil {
			log.Print(err)
			web.HTTPError(rw, err)
			return
		}
		err = validate(&dto)
		if err != nil {
			web.HTTPError(rw, web.ValidationError())
			return
		}

		// Perform action
		org := &Organisation{
			Name:    dto.Name,
			Address: dto.Address,
			Zipcode: dto.Zipcode,
			City:    dto.City,
		}
		org, err = m.Register(r.Context(), org)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Send response
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully registered organisation",
			Data:    org,
		})
	}
}

func (m *Module) httpCreateInvite() http.HandlerFunc {
	// DTO contains required data from the request
	type DTO struct {
		User int `json:"user"`
		// Organisation string `json:"organisation"`
	}

	// validate the DTO
	validate := func(dto *DTO) error {
		return validation.ValidateStruct(
			dto,
			validation.Field(&dto.User, validation.Required),
			// validation.Field(&dto.Organisation, validation.Required),
		)
	}

	// The actual request handler
	return func(rw http.ResponseWriter, r *http.Request) {
		var dto DTO
		_oID := chi.URLParam(r, "orgid")
		oID, err := strconv.ParseInt(_oID, 10, 32)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Decode the DTO from the body
		err = web.DecodeJSON(r, &dto)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Validate DTO
		err = validate(&dto)
		if err != nil {
			web.HTTPError(rw, web.ValidationError())
			return
		}

		// Create invite link
		i, err := m.CreateInviteLink(r.Context(), int(oID), dto.User)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Send response
		web.HTTPResponse(rw, http.StatusCreated, &web.APIResponse{
			Message: "Successfully created invite link",
			Data:    i,
		})
	}
}

func (m *Module) httpAcceptInvite() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		c := chi.URLParam(r, "invitecode")
		_oID := chi.URLParam(r, "orgid")
		oID, err := strconv.ParseInt(_oID, 10, 32)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		err = m.AcceptInvite(r.Context(), int(oID), c)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Successfully joined organisation",
		})
	}
}

func (m *Module) httpRemoveMember() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		_oID := chi.URLParam(r, "orgid")
		oID, err := strconv.ParseInt(_oID, 10, 32)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}
		_mID := chi.URLParam(r, "memberid")
		mID, err := strconv.ParseInt(_mID, 10, 32)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Remove user from org
		err = m.RemoveMember(r.Context(), int(oID), int(mID))
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Response
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully removed user from organisation",
		})
	}
}

func (m *Module) httpGetMembers() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		_oID := chi.URLParam(r, "orgid")
		oID, err := strconv.ParseInt(_oID, 10, 32)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Get members
		members, err := m.GetMembers(r.Context(), int(oID))
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Create response
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully retrieved members for organisation",
			Data:    members,
		})
	}
}

// Router creates router for the user routes
func (m *Module) Router() *chi.Mux {
	r := chi.NewRouter()
	auth := webauth.Create(m.keystore)

	// Protected routes
	r.Group(func(pr chi.Router) {
		pr.Use(auth)

		pr.Get("/", m.httpGetMany())
		pr.Get("/{orgid}", m.httpGetOne())
		pr.Post("/", m.httpCreateOne())
		pr.Post("/{orgid}/invite", m.httpCreateInvite())
		pr.Post("/{orgid}/invite/{invitecode}", m.httpAcceptInvite())
		pr.Get("/{orgid}/members", m.httpGetMembers())
		pr.Delete("/{orgid}/members/{memberid}", m.httpRemoveMember())
	})

	return r
}
