package orgmod

import (
	"time"
)

// Organisation entity
type Organisation struct {
	ID          int       `json:"id"`
	Name        string    `json:"name"`
	Address     string    `json:"address"`
	Zipcode     string    `json:"zipcode"`
	City        string    `json:"city"`
	ArchiveTime uint32    `json:"archiveTime"`
	CreatedAt   time.Time `json:"createdAt"`
}

// OrganisationUserMember is the join table between User and Organisation
type OrganisationUserMember struct {
	OrganisationID int
	UserID         int
}
