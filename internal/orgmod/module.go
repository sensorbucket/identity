package orgmod

import (
	"context"
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/internal/usermod"
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gitlab.com/sensorbucket/identity/pkg/webauth"
)

// Module ...
type Module struct {
	store              Store
	userm              *usermod.Module
	keystore           keystore.Keyprovider
	defaultArchiveTime time.Duration
	logger             *logrus.Entry
}

// Config for this module
type Config struct {
	Store              Store
	UserModule         *usermod.Module
	KeyProvider        keystore.Keyprovider
	DefaultArchiveTime time.Duration
	Logger             *logrus.Logger
}

type inviteClaims struct {
	OrganisationID int `json:"o"`
	UserID         int `json:"u"`
	jwt.StandardClaims
}

// New ...
func New(c *Config) *Module {
	return &Module{
		store:              c.Store,
		userm:              c.UserModule,
		keystore:           c.KeyProvider,
		defaultArchiveTime: c.DefaultArchiveTime,
		logger:             c.Logger.WithField("module", "orgmod"),
	}
}

// Register registers a new organisation
func (m *Module) Register(ctx context.Context, n *Organisation) (*Organisation, error) {
	ctxToken, _ := ctx.Value(webauth.CTXTokenKey).(webauth.Token)

	userToken, isUserToken := ctxToken.(*webauth.UserToken)
	// Must be authenticated as User
	if !isUserToken {
		return nil, ErrInvalidTokenType
	}

	// Enforce default archive time
	if n.ArchiveTime == 0 {
		n.ArchiveTime = uint32(m.defaultArchiveTime.Hours())
	}

	// Set created at
	n.CreatedAt = time.Now().UTC()

	// Ensure there is no organisation with this name already
	existing, err := m.store.FindByName(n.Name)
	if err != nil {
		m.logger.WithField("name", n.Name).WithError(err).Error("organisation registration failed because a store error occured")
		return nil, err
	}
	if existing != nil {
		m.logger.WithField("name", n.Name).WithFields(logrus.Fields{"err": err, "org": existing}).Error("organisation registration failed because an organisation with the same name already exists")
		return nil, DuplicateNameError
	}

	// Create the organisation and register the creator as member
	org, err := m.store.CreateWithOwner(n, userToken.UserID)
	if err != nil {
		m.logger.WithField("name", n.Name).WithError(err).Error("organisation registration failed because a store error occured")
		return nil, err
	}

	return org, nil
}

// GetByID retrieves an organisation by its ID
// if no organisation was found, returns an error
func (m *Module) GetByID(id int) (*Organisation, error) {
	o, err := m.store.FindByID(id)
	if err != nil {
		m.logger.WithField("id", id).WithError(err).Error("could not get organisation by id")
		return nil, err
	}
	return o, nil
}

// GetPage returns all organisations
func (m *Module) GetPage(
	pctx *web.PageContext,
	filter map[string]interface{},
	ordering *web.Ordering,
) ([]*Organisation, *web.PageMetadata, error) {

	// Get page from store
	orgs, page, err := m.store.List(pctx, filter, ordering)
	if err != nil {
		return nil, nil, err
	}
	return orgs, page, nil
}

func (m *Module) GetOrganisationsForUser(uID int) ([]*Organisation, error) {
	return m.store.GetOrganisationsForUser(uID)
}

// CreateInviteLink creates a link which can be used to invite a specific
// user to the organisation
func (m *Module) CreateInviteLink(ctx context.Context, oID int, uID int) (string, error) {
	ctxToken, _ := ctx.Value(webauth.CTXTokenKey).(webauth.Token)

	userToken, isUserToken := ctxToken.(*webauth.UserToken)
	// Must be authenticated as User
	if !isUserToken {
		return "", ErrInvalidTokenType
	}

	// Verify that both the user and the organisation exist
	// This happens in parallel for (hopefully) better performance
	// the errchan receives exactly 1 error response from BOTH requests
	// If one of the responses is NOT nil then this function returns that error
	errChan := make(chan error)

	// Fetch the organisation by its ID
	go func() {
		_, err := m.GetByID(oID)
		if err != nil {
			errChan <- err
			return
		}
		errChan <- nil
	}()

	// Fetch the user by its ID
	go func() {
		_, err := m.userm.GetByID(uID)
		if err != nil {
			errChan <- err
			return
		}
		errChan <- nil
	}()

	// Expect two responses
	// Could this possible deadlock?
	for i := 0; i < 2; i++ {
		if err := <-errChan; err != nil {
			return "", err
		}
	}

	// Check if the actor is a member of the organisation
	isMember, err := m.store.HasMember(oID, userToken.UserID)
	if err != nil {
		return "", err
	}
	if !isMember {
		return "", web.ForbiddenError()
	}

	// Get latest private key
	key := m.keystore.GetLatest()
	if key == nil {
		return "", errors.New("No private key in keystore")
	}
	privkey := key.RSAPrivateKey()
	if privkey == nil {
		return "", errors.New("Latest key is not an RSA private key")
	}

	// Create the correct claims for this token and then create and sign the
	// JSON Web Token using the private key from the passed config
	claims := &inviteClaims{
		OrganisationID: oID,
		UserID:         uID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(1e9 * 60 * 60 * 24).Unix(),
		},
	}
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	// Add the key ID with which the token will be signed
	t.Header["kid"] = key.ID

	// Sign the json web token
	tStr, err := t.SignedString(privkey)
	if err != nil {
		return "", err
	}

	return tStr, nil
}

// AcceptInvite accepts an invite code and adds the user to the organisation
// as a member
func (m *Module) AcceptInvite(ctx context.Context, oID int, token string) error {
	ctxToken, _ := ctx.Value(webauth.CTXTokenKey).(webauth.Token)

	userToken, isUserToken := ctxToken.(*webauth.UserToken)
	// Must be authenticated as User
	if !isUserToken {
		return ErrInvalidTokenType
	}

	// Decode the JWT and verify the validity.
	// If signing algorithm is not set to RSA, fail directly because this may be
	// an attempted "attack"
	// https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/
	t, err := jwt.ParseWithClaims(token, &inviteClaims{}, m.jwtKeyFunc)
	if err != nil {
		if verr, ok := err.(*jwt.ValidationError); ok {
			if (verr.Errors & jwt.ValidationErrorExpired) != 0 {
				return ExpiredInviteTokenError
			}
		}
		return InvalidInviteTokenError
	}

	// Token is parsed, make sure the correct claims exist and that the token is
	// valid
	ic, ok := t.Claims.(*inviteClaims)
	if !ok || !t.Valid {
		return InvalidInviteTokenError
	}

	// Check if the invite is actually for the currently authenticated user
	if userToken.UserID != ic.UserID {
		return web.ForbiddenError()
	}

	// Verify that the user is not already a member of this organisation
	alreadyMember, err := m.store.HasMember(ic.OrganisationID, ic.UserID)
	if err != nil {
		return err
	}
	if alreadyMember {
		return AlreadyAMemberError
	}

	// Create the organisation user membership and store it
	membership := &OrganisationUserMember{
		OrganisationID: ic.OrganisationID,
		UserID:         ic.UserID,
	}

	err = m.store.AddMember(membership)
	if err != nil {
		return err
	}

	return nil
}

// RemoveMember removes a member from the organisation
func (m *Module) RemoveMember(ctx context.Context, oID int, mID int) error {
	ctxToken, _ := ctx.Value(webauth.CTXTokenKey).(webauth.Token)

	userToken, isUserToken := ctxToken.(*webauth.UserToken)
	// Must be authenticated as User
	if !isUserToken {
		return ErrInvalidTokenType
	}

	// User is leaving the organisation
	if userToken.UserID == mID {
		err := m.store.RemoveMember(oID, userToken.UserID)
		if err != nil {
			return err
		}
	} else {
		return web.ForbiddenError()
	}

	return nil
}

// GetMembers returns all users that belong to the given organisation
func (m *Module) GetMembers(ctx context.Context, oID int) ([]*usermod.User, error) {
	ctxToken, _ := ctx.Value(webauth.CTXTokenKey).(webauth.Token)

	// Token must have access
	if !ctxToken.CanAccessOrganisation(oID) {
		return nil, web.ForbiddenError()
	}

	members, err := m.store.GetMembers(oID)
	if err != nil {
		return nil, err
	}

	return members, nil
}

func (m *Module) jwtKeyFunc(t *jwt.Token) (interface{}, error) {
	// Check if the key was signed with an RSA key
	if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
		return nil, InvalidInviteTokenError
	}

	// Check if a Key ID is set, this is required to fetch the correct key from
	// the keystore
	kid, ok := t.Header["kid"].(string)
	if !ok || kid == "" {
		return nil, InvalidInviteTokenError
	}

	// Get the generic key for the key ID
	key := m.keystore.Get(kid)
	if key == nil {
		return nil, InvalidInviteTokenError
	}

	// Use it as public key. If this returns nil then the key associated with
	// the Key ID is not an RSA key
	pubkey := key.RSAPublicKey()
	if pubkey == nil {
		return nil, InvalidInviteTokenError
	}

	return pubkey, nil
}
