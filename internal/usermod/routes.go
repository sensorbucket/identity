package usermod

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gitlab.com/sensorbucket/identity/pkg/webauth"
)

func (m *Module) httpRegisterUser() http.HandlerFunc {
	// DTO represents the data expected in the request for this function
	type DTO struct {
		Email     string `json:"email"`
		Firstname string `json:"firstname"`
		Lastname  string `json:"lastname"`
		Password  string `json:"password"`
	}

	// Validation logic for the DTO
	validate := func(dto *DTO) error {
		return validation.ValidateStruct(
			dto,
			validation.Field(
				&dto.Email,
				validation.Required,
				is.Email,
			),
			validation.Field(&dto.Firstname, validation.Required),
			validation.Field(&dto.Lastname, validation.Required),
			validation.Field(&dto.Password, validation.Required),
		)
	}

	// The actual HTTP handler
	return func(rw http.ResponseWriter, r *http.Request) {
		var dto DTO

		// Decode the DTO from the body and return an error if present
		err := web.DecodeJSON(r, &dto)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Validate the decoded DTO and return an error if present
		err = validate(&dto)
		if err != nil {
			web.HTTPError(rw, web.ValidationError())
			return
		}

		// Perform registration
		u := &User{
			Email:     dto.Email,
			Firstname: dto.Firstname,
			Lastname:  dto.Lastname,
			// Password is set seperately in `Module.RegisterUser`
		}
		u, err = m.RegisterUser(u, dto.Password)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Respond to request
		web.HTTPResponse(rw, http.StatusCreated, &web.APIResponse{
			Message: "Succesfully registered user",
			Data:    u,
		})
	}
}

func (m *Module) httpGetSelf() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		// Get user ID from request context
		ctxToken, _ := r.Context().Value(webauth.CTXTokenKey).(webauth.Token)

		userToken, isUserToken := ctxToken.(*webauth.UserToken)
		// Must be authenticated as User
		if !isUserToken {
			web.HTTPError(rw, ErrInvalidTokenType)
			return
		}

		u, err := m.GetByID(userToken.UserID)
		if err != nil {
			web.HTTPError(rw, err)
			return
		}

		// Respond with user id
		web.HTTPResponse(rw, http.StatusOK, &web.APIResponse{
			Message: "Succesfully fetched user",
			Data:    u,
		})
	}
}

// Routes creates router for the user routes
func (m *Module) Routes() *chi.Mux {
	r := chi.NewRouter()
	auth := webauth.Create(m.keyProvider)

	// Public routes
	r.Post("/", m.httpRegisterUser())

	// Protected routes
	r.Group(func(pr chi.Router) {
		pr.Use(auth)
		pr.Get("/self", m.httpGetSelf())
	})

	return r
}
