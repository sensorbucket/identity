package usermod

import (
	"time"

	"gitlab.com/sensorbucket/identity/pkg/keystore"
)

// Module contains dependencies for user module
type Module struct {
	name        string
	store       Store
	keyProvider keystore.Keyprovider
}

// Config contains require
type Config struct {
	Store       Store
	KeyProvider keystore.Keyprovider
}

// New creates a new users module
func New(c *Config) *Module {
	return &Module{
		store:       c.Store,
		keyProvider: c.KeyProvider,
	}
}

// GetByID gets a user by its ID, returns an error if the user was not found
func (m *Module) GetByID(id int) (*User, error) {
	u, err := m.store.GetUserByID(id)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, ErrNotFound
	}
	return u, nil
}

// GetByEmail gets a user by its email, returns an error if the user was not found
func (m *Module) GetByEmail(email string) (*User, error) {
	u, err := m.store.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, ErrNotFound
	}
	return u, nil
}

// RegisterUser ...
func (m *Module) RegisterUser(u *User, p string) (*User, error) {
	// Set created at
	u.CreatedAt = time.Now().UTC()

	// Hash and set user password
	u.HashAndSetPassword(p)

	// Set user as active
	// TODO: Email confirmation procedure? See `send email` below
	u.Active = true

	// Email already in use?
	exists, err := m.store.GetUserByEmail(u.Email)
	if err != nil {
		return nil, err
	}
	if exists != nil {
		return nil, ErrEmailAlreadyInUse
	}

	// Store user
	u, err = m.store.CreateUser(u)
	if err != nil {
		return nil, err
	}

	// Send email
	// err = smtp.SendMail(
	// 	"127.0.0.1:2500",
	// 	nil,
	// 	"info@sensorbucket.nl",
	// 	[]string{
	// 		u.Email,
	// 	},
	// 	[]byte(
	// 		"To:"+u.Email+"\r\n"+
	// 			"Subject: Registration at Sensorbucket\r\n"+
	// 			"Content-Type: text/html;\r\n"+
	// 			"\r\n"+
	// 			`
	// 			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	// 			<html xmlns="http://www.w3.org/1999/xhtml">
	// 			<head>
	// 				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	// 				<title>Registration at Sensorbucket</title>
	// 				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	// 			</head>
	// 			<body>
	// 				<p>
	// 				Thank you for your registration at Sensorbucket.</br>
	// 				To confirm your account please click the following link:</br>
	// 				</br>
	// 				<a href="https://google.nl/">Confirm</a></br>
	// 				</br>
	// 				Have fun!
	// 				</p>
	// 			</body>
	// 			</html>
	// 	`),
	// )
	// if err != nil {
	// 	log.Printf("SMTP Error: %v", err)
	// }

	return u, nil
}
