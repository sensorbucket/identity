package usermod

import (
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// User entity
type User struct {
	ID        int       `json:"id"`
	Email     string    `json:"mail"`
	Password  string    `json:"-"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	CreatedAt time.Time `json:"-"`
	Active    bool      `json:"-"`
	SuperUser bool      `json:"-"`
}

// HashAndSetPassword ...
func (u *User) HashAndSetPassword(p string) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(p), 5)
	u.Password = string(hash)
}

// ComparePassword checks if the given password `p` equals the stored hash
func (u *User) ComparePassword(p string) error {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(p))
	isInvalid := errors.Is(err, bcrypt.ErrMismatchedHashAndPassword)
	if isInvalid {
		return ErrInvalidUserPassword
	}
	return err
}
