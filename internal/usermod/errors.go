package usermod

import (
	"net/http"

	"gitlab.com/sensorbucket/identity/pkg/web"
)

var (
	// ErrEmailAlreadyInUse ...
	ErrEmailAlreadyInUse = &web.APIError{
		Message:    "The provided email address is already in use",
		Code:       "DUPLICATE_USER_EMAIL",
		HTTPStatus: http.StatusBadRequest,
	}

	// ErrNotFound ...
	ErrNotFound = &web.APIError{
		Message:    "User was not found",
		Code:       "USER_NOT_FOUND",
		HTTPStatus: http.StatusNotFound,
	}

	// ErrInvalidUserPassword ...
	ErrInvalidUserPassword = &web.APIError{
		Message:    "Invalid user password",
		Code:       "USER_INVALID_PASSWORD",
		HTTPStatus: http.StatusForbidden,
	}

	ErrInvalidTokenType = &web.APIError{
		Message:    "Token is not meant for this resource",
		Code:       "ORGANISATION_INVALID_AUTH_TOKEN",
		HTTPStatus: 401,
	}
)
