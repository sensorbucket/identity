package usermod

import (
	"gorm.io/gorm"
)

// Store ...
type Store interface {
	CreateUser(u *User) (*User, error)
	GetUserByID(id int) (*User, error)
	GetUserByEmail(e string) (*User, error)
}

// GormStore ...
type GormStore struct {
	db *gorm.DB
}

// NewGormStore creates a new Postgres store
func NewGormStore(db *gorm.DB) *GormStore {
	return &GormStore{
		db: db,
	}
}

// CreateUser ...
func (s *GormStore) CreateUser(u *User) (*User, error) {
	res := s.db.Create(u)

	return u, res.Error
}

// GetUserByID ...
func (s *GormStore) GetUserByID(id int) (*User, error) {
	var u User
	res := s.db.First(&u, id)

	if res.Error == gorm.ErrRecordNotFound {
		return nil, nil
	}

	return &u, res.Error
}

// GetUserByEmail ...
func (s *GormStore) GetUserByEmail(email string) (*User, error) {
	var u User
	res := s.db.First(&u, "email = ?", email)

	if res.Error == gorm.ErrRecordNotFound {
		return nil, nil
	}

	return &u, res.Error
}
