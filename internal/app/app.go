package app

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/internal/authmod"
	"gitlab.com/sensorbucket/identity/internal/orgmod"
	"gitlab.com/sensorbucket/identity/internal/usermod"
	"gitlab.com/sensorbucket/identity/pkg/keystore"
	"gitlab.com/sensorbucket/identity/pkg/web"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// App represents the application
type App struct {
	logger      *logrus.Logger
	server      *web.Server
	db          *gorm.DB
	keyprovider keystore.Keyprovider
}

func (a *App) connectDatabase(url string) {
	// Create connection
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: url,
	}))
	if err != nil {
		a.logger.Fatalf("Database connection could not be created: %s\n", err)
	}

	a.logger.Info("Database connection succesful\n")
	a.db = db
}

func corsMiddleware() func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		mw := func(rw http.ResponseWriter, r *http.Request) {
			// Set CORS headers
			if origin := r.Header.Get("Origin"); origin != "" {
				rw.Header().Set("Access-Control-Allow-Origin", origin)
				rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
				rw.Header().Set("Access-Control-Allow-Headers", "Accept, Accept-Language, Authorization, Content-Type")
			}

			// Allow Preflight options
			if r.Method == "OPTIONS" {
				return
			}

			h.ServeHTTP(rw, r)
		}
		return http.HandlerFunc(mw)
	}
}

// Start the identity app
func Start(c *Config) {
	var err error

	// Capture OS Signals to gracefully exit the program
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT)

	app := &App{
		logger: c.Logger,
	}

	// Configure and create database connection
	app.connectDatabase(c.DatabaseURL)

	// Configure keystore
	app.createKeystore(c)

	// Configure webserver
	app.server = web.NewServer(
		c.HTTP.Addr,
		c.HTTP.Port,
	)
	app.server.Router.Use(corsMiddleware())
	serverErrChan := app.server.Listen()
	app.logger.Infof("HTTP Server listening at %s", app.server.HTTP.Addr)

	// ========================
	//  Register modules
	// ========================

	// Users module
	userModule := usermod.New(&usermod.Config{
		Store:       usermod.NewGormStore(app.db),
		KeyProvider: app.keyprovider,
	})
	app.server.Mount("/users", userModule.Routes())

	// Organisation module
	orgModule := orgmod.New(&orgmod.Config{
		Logger:             app.logger,
		Store:              orgmod.NewGormStore(app.db),
		UserModule:         userModule,
		KeyProvider:        app.keyprovider,
		DefaultArchiveTime: 7 * 24 * time.Hour,
	})
	app.server.Mount("/organisations", orgModule.Router())

	// Authentication module
	authModule := authmod.New(&authmod.Config{
		Logger:      app.logger,
		UserModule:  userModule,
		OrgModule:   orgModule,
		KeyProvider: app.keyprovider,
	})
	app.server.Mount("/auth", authModule.Routes())

	// ========================
	//  Set server to healthy and handle errors
	// ========================

	// Set server healthy
	app.server.SetHealthy(true)
	app.logger.Info("HTTP server set to healthy")

	select {
	case err = <-serverErrChan:
		app.logger.Errorf("Server error occured: %s", err)
		app.stop()
	case <-sigChan:
		app.logger.Errorf("Shutting down...")
		app.stop()
		return
	}
}

func (a *App) stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	a.server.Shutdown(ctx)
}
