package app

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/identity/pkg/web"
)

// Config for the application
type Config struct {
	IsProduction   bool
	HTTP           web.Config
	DatabaseURL    string
	JWKSURI        string
	PrivateKeyPath string
	Hostname       string
	Logger         *logrus.Logger
}
