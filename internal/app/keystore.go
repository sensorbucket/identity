package app

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"

	"gitlab.com/sensorbucket/identity/pkg/keystore"
)

func (a *App) createKeystore(c *Config) {

	// If a JWKSURI is set then create a JWKS keystore
	if c.JWKSURI != "" {
		// Create the keystore
		ks := keystore.NewJWKSKeystore(c.JWKSURI)

		// Immidiatly fetch the latest keys to assure that atleast some
		// keys are available for use
		err := ks.Update()
		if err != nil {
			a.logger.Fatalf("Could not update JWKS keystore: %s", err)
		}

		a.keyprovider = ks
		return
	}

	// If no JWKSURI is set then a local file MUST be set
	// Read keyfile, assume it is RSA private key
	dat, err := ioutil.ReadFile(c.PrivateKeyPath)
	if err != nil {
		a.logger.
			WithField("keypath", c.PrivateKeyPath).
			Fatalf("Could not read key file for local keystore. Does the file at exist? %s", err)
	}

	// Decode PEM to RSA key
	b, _ := pem.Decode(dat)
	key, err := x509.ParsePKCS1PrivateKey(b.Bytes)
	if err != nil {
		a.logger.
			WithField("keypath", c.PrivateKeyPath).
			Fatalf("Could not parse private key from keyfile. Is it a RSA key in PEM format? %s", err)
	}

	ks := keystore.NewStaticKeystore()
	ks.Add("static", key)
	a.keyprovider = ks
}
